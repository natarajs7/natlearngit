package testCases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class Project extends SeMethods
{
	@Test
	public void MergeContact() throws InterruptedException
	{
		startApp("chrome", "http://leaftaps.com/opentaps");
		
		WebElement eleUsername = locateElement("id","username"); // Locate Username field
		type(eleUsername, "DemoSalesManager"); //Enter Used Id
		
		verifyPartialAttribute(eleUsername, "class", "Login"); // To verify the attribute value with the user given value partially
		
		WebElement elePassword = locateElement("id", "password"); // Locate Password field
		type(elePassword, "crmsfa"); // Enter Password
		
		verifyExactAttribute(elePassword, "type", "Password"); // To verify the attribute value with the user given value exactly
		
		WebElement eleLogin = locateElement("classname", "decorativeSubmit"); //Locate Login button
		click(eleLogin); // Click the Login Button
		
		WebElement eleCRMSFA = locateElement("LinkText", "CRM/SFA"); //Locate CRM/SFA link
		click(eleCRMSFA); //click on the CRM/SFA link
		
		WebElement eleAccountLink = locateElement("xpath", "//a[text()='Accounts']");
		click(eleAccountLink);
		
		WebElement eleMergeAccount = locateElement("xpath", "//a[text()='Merge Accounts']");
		click(eleMergeAccount);
		
		WebElement eleFromAccount = locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[1]");
		click(eleFromAccount);
		
		switchToWindow(1);
		
		WebElement eleFromAccountID = locateElement("xpath", "//input[@name='id']");
		type(eleFromAccountID, "10601");
		
		WebElement eleFindAccountButton = locateElement("xpath", "(//button[@class='x-btn-text'])[1]");
		click(eleFindAccountButton);
		
		Thread.sleep(500);
		
		WebElement eleFirstAccount = locateElement("xpath", "(//a[@class='linktext'])[1]");
		clickWithoutSnap(eleFirstAccount);
		
		switchToWindow(0);
		
		WebElement eleToAccount = locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[2]");
		click(eleToAccount);
		
		switchToWindow(1);
		
		WebElement eleToAccountID = locateElement("xpath", "//input[@name='id']");
		type(eleToAccountID, "10602");
		
				
		WebElement eleToFindAccountButton = locateElement("xpath", "//button[text()='Find Accounts']");
		click(eleToFindAccountButton);
		Thread.sleep(500);
		
		WebElement eleToFirstResult = locateElement("xpath", "(//a[@class='linktext'])[1]");
		clickWithoutSnap(eleToFirstResult);
		
		switchToWindow(0);
		
		WebElement eleMergeButton = locateElement("xpath", "//a[@class='buttonDangerous']");
		clickWithoutSnap(eleMergeButton);
		
		acceptAlert();
		
		WebElement eleFindAccountlink = locateElement("xpath", "//a[text()='Find Accounts']");
		click(eleFindAccountlink);
		
		WebElement eleMergedFirstAccountID = locateElement("xpath", "//input[@name='id']");
		type(eleMergedFirstAccountID, "10601");
		
		WebElement eleMergedFindAccountButton = locateElement("xpath", "//button[text()='Find Accounts']");
		click(eleMergedFindAccountButton);
		
		WebElement eleErrorMessage = locateElement("xpath", "//div[text()='No records to display']");
		verifyExactText(eleErrorMessage, "No records to display");
		
		closeBrowser();
		
		
		
		
		
		
		
		
	}

}


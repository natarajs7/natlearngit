package testCases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class DeleteLead extends ProjectSpecificMethods
{
	/*@Test(enabled=false)*/
	@Test(groups="Regression",dependsOnGroups="Sanity")
	
	public void Delete() throws InterruptedException 
	{
		WebElement eleLeads = locateElement("xpath", "//a[text()='Leads']"); //Locate Leads link
		click(eleLeads); // Click Leads Link

		WebElement eleFindLeads = locateElement("LinkText", "Find Leads");//Locate Find Leads link
		click(eleFindLeads); // click Find Leads link
		
		WebElement elePhoneLink = locateElement("xpath", "(//span[@class='x-tab-strip-text '])[2]");
		click(elePhoneLink);
		
		/*WebElement elePhoneCountryCode = locateElement("xpath", "//input[@name='phoneCountryCode']");
		elePhoneCountryCode.clear();
		type(elePhoneCountryCode, "91");
		
		WebElement elePhoneAreaCode = locateElement("xpath", "//input[@name='phoneAreaCode']");
		type(elePhoneAreaCode, "471");
		*/
		WebElement elePhoneNumber = locateElement("xpath", "//input[@name='phoneNumber']");
		type(elePhoneNumber, "914712462938");
		
		WebElement eleFindLeadsButton = locateElement("xpath", "(//button[@class='x-btn-text'])[7]");
		click(eleFindLeadsButton);
		
		Thread.sleep(2000);
		
		WebElement eleFirstResult = locateElement("xpath", "(//a[@class='linktext'])[4]");
		getText(eleFirstResult);
		click(eleFirstResult);
		
		WebElement eleDeleteButton = locateElement("xpath", "//a[text()='Delete']");
		click(eleDeleteButton);

		WebElement eleFindLeadsLink = locateElement("LinkText", "Find Leads");//Locate Find Leads link
		click(eleFindLeadsLink); // click Find Leads link
		
		WebElement eleLeadID = locateElement("xpath", "(//input[@type='text'])[29]");
		type(eleLeadID, eleFirstResult.getText());

	}
}

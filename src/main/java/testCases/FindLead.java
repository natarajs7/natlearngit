package testCases;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class FindLead extends ProjectSpecificMethods
{
	@BeforeClass
	public void setData() 
	{
		testCaseName = "TC02_FindLead";
		testDesc = "Edit a lead";
		author = "Koushik";
		category  = "Sanity";
	}

	@Test
	public void findLeadsTC()
	{
		click(locateElement("link", "CRM/SFA"));
		click(locateElement("link", "Leads"));
		click(locateElement("link", "Find Leads"));
		type(locateElement("xpath", "(//input[@name='firstName'])[3]"), "ko");
		click(locateElement("xpath", "//button[text()='Find Leads']"));
	}
}

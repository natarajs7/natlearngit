package testCases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


public class EditLead extends ProjectSpecificMethods
{
	@BeforeTest(groups="Sanity")
	public void setData() {
		testCaseName = "Edit Lead"; 
		testDesc = "Edit the existing Lead in Leaftaps";
		author="Anupam";
		category="Smoke";

	}


	//@Test(dependsOnMethods = "testCases.CreateLead.create") //To run the Test Csase without main method

	@Test(groups="Sanity" /*,dependsOnGroups="Smoke"*/)

	public void Edit() throws InterruptedException
	{
		WebElement eleLeads = locateElement("xpath", "//a[text()='Leads']"); //Locate Leads link
		click(eleLeads); // Click Leads Link

		WebElement eleFindLeads = locateElement("LinkText", "Find Leads");//Locate Find Leads link
		click(eleFindLeads); // click Find Leads link

		WebElement eleFirstName = locateElement("xpath", "(//input [@name='firstName'])[3]"); //Locate First Name field
		type(eleFirstName, "Nataraj"); //Enter the First Name

		WebElement eleFindLeadsButton = locateElement("xpath", "(//button[@class='x-btn-text'])[7]"); //Locate Find Leads button
		click(eleFindLeadsButton); //click the Find Leads button

		Thread.sleep(2000); // allow 2 seconds to load the results

		WebElement eleFirstResult = locateElement("xpath", "(//a[@class='linktext'])[4]"); //Locate the First Resulting Lead
		click(eleFirstResult);

		verifyExactTitle("View Lead | opentaps CRM"); //Verify the Title of the Web Page

		WebElement eleEditButton = locateElement("xpath", "(//a[@class='subMenuButton'])[3]"); // Locate the Edit button
		click(eleEditButton);

		WebElement eleCompanyName = locateElement("id", "updateLeadForm_companyName");
		eleCompanyName.clear(); // to clear the field
		type(eleCompanyName, "IBM");

		WebElement eleUpdateButton = locateElement("xpath", "//input[@value='Update']");
		click(eleUpdateButton);

		WebElement eleText = locateElement("id", "viewLead_companyName_sp");
		verifyExactText(eleText, "IBM (10206)");

	}
}

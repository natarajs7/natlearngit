package testCases;


import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import Excel.LearnExcel;

public class CreateLead extends ProjectSpecificMethods

{

	@BeforeTest(groups="common")

	public void setData() {
		testCaseName = "TC01_Create Lead"; 
		testDesc = "Create a new Lead in Leaftaps";
		author="Nataraj";
		category="Smoke";
		excelfilename = "CreateLead";

	}

	//@Test(invocationCount=1, invocationTimeOut=30000) // @Test - To run the Test Case without Main Method

	//@Test(groups="Smoke") //

	@Test (dataProvider = "fetchdata")

	public void create(String cname, String fname, String lname) 

	{

		WebElement eleCrateLead = locateElement("LinkText", "Create Lead"); //Locate Create Lead link
		click(eleCrateLead); //click on the Create Lead links 

		WebElement eleCompanyName = locateElement("id", "createLeadForm_companyName"); //Locate Company Name
		type(eleCompanyName, cname);

		WebElement eleFirstName = locateElement("id", "createLeadForm_firstName");
		type(eleFirstName, fname);

		WebElement eleLastName = locateElement("id", "createLeadForm_lastName");
		type(eleLastName, lname);

		WebElement eleCreateLeadButton = locateElement("name", "submitButton");
		verifyDisplayed(eleCreateLeadButton);
		click(eleCreateLeadButton);

	}	
	/*@DataProvider(name = "fetchdata")
	public static Object[][] getData() throws IOException 
	{
		
		
		
		String[][] data = new String[4][3];

		data[0][0] = "IBM";
		data[0][1] = "Nataraj";
		data[0][2] = "S";

		data[1][0] = "IBM";
		data[1][1] = "Anupam";
		data[1][2] = "Shankar";

		data[2][0] = "IBM";
		data[2][1] = "Kannan";
		data[2][2] = "A";

		data[3][0] = "IBM";
		data[3][1] = "Indhumathi";
		data[3][2] = "A";


		return LearnExcel.setData();
	}
*/
}